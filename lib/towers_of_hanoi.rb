

class TowersOfHanoi

attr_reader :towers

  def initialize
    @towers = [ [3, 2, 1], [], [] ]
  end

  def play
    puts %Q(Welcome to "Towers of Hanoi"!)

    while won? == false
      puts %Q(What pile would you like to select from? Current pile inventory is #{@towers})
      render
    end
    puts %Q(Congratulations! You won!)
  end

  def render
    input_a = gets.to_i
    tower_a = @towers[input_a]
    if tower_a.empty?
      while tower_a.empty?
        puts "I'm sorry this pile is currently empty :("
        puts "Please choose another one!"
        tower_a = @towers[gets.to_i]
      end
    end
    puts %Q(And what tower would you like to move a disc too?)
    input_b = gets.to_i
    tower_b = @towers[input_b]
    while valid_move?(input_a, input_b) == false
      puts "I'm sorry, that is not a valid move :("
      puts "Please choose another one!"
      input_b = gets
    end
    move(input_a, input_b)
  end

  def move(tower_a, tower_b)
    disc = @towers[tower_a].pop
    @towers[tower_b].push(disc)
  end

  def valid_move?(tower_a, tower_b)
    if @towers[tower_a].empty?
      boolean = false
    else
      move(tower_a, tower_b)
      boolean = @towers[tower_b] == @towers[tower_b].sort.reverse
      move(tower_b, tower_a)
    end
    boolean
  end

  def won?
    @towers[1] == [3, 2, 1] || @towers[2] == [3, 2, 1]
  end
end
